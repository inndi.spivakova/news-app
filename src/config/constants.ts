export const ROUTES = Object.freeze({
  notFound: './not-found',
  savedNews: '/saved-news',
  main: '/',
});

export const LIMIT: number = 3;
